create database dbo;

use dbo;

create table dbo.Department
(
  DepartmentId int not null,
  DepartmentName varchar(100) not null,
  primary key (departmentId)
);

insert into dbo.Department values 
(1, 'sales' ), 
(2, 'finance' ),
(3, 'IT' ),
(4, 'HR' );



create table dbo.Employee 
(
  EmployeeId int not null,
  EmployeeName varchar(100) not null,
  DateofJoining date,
  Department varchar(500) not null,
  PhotoFileName varchar(500),
  primary key (EmployeeId)
);

insert into Employee values  
(1, 'ram','2020-06-12','sales','anaonmous.png'),
(2, 'siri', '2020-06-12','HR','siri.png'),
(3, 'ani', '2020-06-12','HR','venu.png'),
(4, 'anu', '2020-06-12','HR','srikanth.png');